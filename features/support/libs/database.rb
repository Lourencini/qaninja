require "pg"

class Database
  
  def initialize
    @connection = PG.connect(host: '127.0.0.1', dbname: 'ninjaflix', user: 'postgres', password: 'qaninja')
  end

  def delete_movie(title)
    @connection.exec("DELETE from public.movies where title = '#{title}';")
  end

  def insert_movie(movie)

    # A \ no fim da linha é para indicar que a segunda linha é continuação da primeira (Mesma String), adicionar um espaço no inicio da segunda linha
    sql_script = "INSERT INTO public.movies (title, status, year, release_date, created_at, updated_at)" \
                 " VALUES('#{movie['title']}', '#{movie['status']}', '#{movie['year']}', '#{movie['release_date']}', current_timestamp, current_timestamp);"
    
    @connection.exec(sql_script)
  end

 
end