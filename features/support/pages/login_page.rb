class LoginPage
    include Capybara::DSL # para incluir os recursos do capybara

    def go
      visit '/'        
    end

    def with(email, pass)
        find('#emailId').set email
        find("input[name=password]").set pass
        click_button "Entrar"
    end    

    def alert
      find('.alert').text
    end
end