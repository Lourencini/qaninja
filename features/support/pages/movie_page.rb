class  MoviePage
  include Capybara::DSL

  def add
    find(".movie-add").click
  end

  def upload(file)

    cover_file = File.join(Dir.pwd, "features/support/fixtures/cover/#{file}") #utilizando o Dir.pwd pega o elemento a partir da raiz do projeto
    cover_file = cover_file.tr("/","\\") if OS.windows? # para o windows que a utiliza barra invertida, devemos utilizar esse método para conversão da barra, utilizamos a gem OS para essa verificação.

    Capybara.ignore_hidden_elements = false #Aqui passo a informação para o capybara ignora o hidden no elemento do css.
    attach_file('upcover', cover_file)
    Capybara.ignore_hidden_elements = true #Aqui volta o padrão para verificar o hidden do css

  end

  def add_cast(cast)
    actor = find('.input-new-tag')
    cast.each do |a|
      actor.set a
      actor.send_keys :tab  
    end
  end

  def alert
    find(".alert").text
  end

  def select_status(status)
    find('input[placeholder=Status]').click 
    find('.el-select-dropdown__item', text: status).click
  end
  
  def create(movie)
    find('input[name=title]').set movie["title"]

    find('input[name=year]').set movie["year"]
    find('input[name=release_date]').set movie["release_date"]  

    select_status(movie["status"]) unless movie["status"].empty?

    add_cast(movie["cast"])
    
    find('textarea[name=overview]').set movie["overview"]
    
    upload(movie["cover"]) unless movie["cover"].empty?

    find("#create-movie").click
  end

  def movie_tr(title)
    find("table tbody tr", text: title)
  end

  def remove(title)
    movie_tr(title).find(".btn-trash").click
  end

  def swal2_confirm
    find('.swal2-confirm').click
  end

  def has_no_movie(title)
    page.has_no_css?("table tbody tr", text: title)
  end 
end
