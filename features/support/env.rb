require "capybara"
require "capybara/cucumber"
require "selenium-webdriver"
require "os"
require "database_cleaner"

require_relative "helpers"

World(Helpers) #define com padrão todos os métodos que são criados no helpers.rb

################################ config para o headless #####################################
# Capybara.register_driver :chrome do |app|
#    Capybara::Selenium::Driver.new app, browser: :chrome,
#      options: Selenium::WebDriver::Chrome::Options.new(args: %w[headless disable-gpu])
#  end
  
#  Capybara.javascript_driver = :chrome

Capybara.configure do |config|
    config.default_driver = :selenium # Utilizando o Firefox
    #config.default_driver = :selenium_chrome # Utilizando o Chrome
    #config.default_driver = :chrome # Utilizando o healess
    config.app_host = "http://localhost:8080" #endereço da aplicação no docker
    config.default_max_wait_time = 10 #aumenta o tempo de esperar para localizar um elemento na página.
end